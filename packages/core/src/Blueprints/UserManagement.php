<?php

namespace MaxTor\Core\Blueprints;

use Illuminate\Database\Schema\Blueprint;

class UserManagement
{
    /**
     * The name of default created by user id column.
     */
    const CREATED_BY = 'created_user_id';

    /**
     * The name of default modified by user id  column.
     */
    const MODIFIED_BY = 'modified_user_id';

    /**
     * The name of default permission id column.
     */
    const PERMISSION_ID = 'permission_id';

    /**
     * The name of default role id column.
     */
    const ROLE_ID = 'role_id';

    /**
     * The name of default locked column.
     */
    const LOCKED = 'locked';

    /**
     * The name of default enabled column.
     */
    const ENABLED = 'enabled';

    /**
     * Add default columns to the table. Also create an index.
     *
     * @param Blueprint $table
     */
    public static function columns(Blueprint $table): void
    {
        $table->unsignedInteger(self::PERMISSION_ID)->nullable();
        $table->unsignedInteger(self::ROLE_ID)->nullable();

        $table->unsignedInteger(self::CREATED_BY)->nullable();
        $table->unsignedInteger(self::MODIFIED_BY)->nullable();
        $table->boolean(self::LOCKED)->default(false)->comment('Заблокированный от изменений');
        $table->boolean(self::ENABLED)->default(true)->comment('Включено');
    }

    /**
     * Drop columns.
     *
     * @param Blueprint $table
     */
    public static function dropColumns(Blueprint $table): void
    {
        $columns = static::getDefaultColumns();
        $table->dropIndex($columns);
        $table->dropColumn($columns);
    }

    /**
     * Get a list of default columns.
     *
     * @return array
     */
    public static function getDefaultColumns(): array
    {
        return [
            static::CREATED_BY,
            static::MODIFIED_BY,
            static::PERMISSION_ID,
            static::ROLE_ID,
            static::ENABLED,
        ];
    }
}
