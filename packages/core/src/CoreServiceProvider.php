<?php

namespace MaxTor\Core;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\ServiceProvider;
use MaxTor\Core\Blueprints\UserManagement;

class CoreServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
//        $this->loadViewsFrom(mxtcore_resources_path('/views/'), 'mxtcore');
//        $this->loadRoutesFrom(mxtcore_path('/routes.php'));
//
//        $this->publishes([
//            mxtcore_config_path('/config/mxtcore.php') => config_path('mxtcore.php'),
//        ]);

        //Migration
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
    }

    public function register(): void
    {
        Blueprint::macro('userManagement', function () {
            UserManagement::columns($this);
        });
    }
}
