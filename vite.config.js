const Dotenv = require('dotenv');
import vue from '@vitejs/plugin-vue'

Dotenv.config();

export default ({ command }) => ({
    plugins: [vue()],
    root: 'resources',
    base: command === 'serve'
        ? ''
        : '/build/',
    publicDir: 'fake_dir_so_nothing_gets_copied',
    build: {
        outDir: 'public/build',
        emptyOutDir: true,
        manifest: true,
        target: 'es2018',
        rollupOptions: {
            input: '/js/app.js',
        },
    },

    resolve: {
        alias: {
            '@': '/js',
        }
    },

    optimizeDeps: {
        include: [
            'vue',
            'axios'
        ]
    }
});
